import React, {Component} from 'react';
import {connect} from 'react-redux';
import {saveComment, fetchComments} from 'actions';

class CommentBox extends Component {
    state = {comment: ''}

    handleCange = event => {
        this.setState({comment: event.target.value});
    }

    handleSubmit = event => {
        event.preventDefault();
        this.props.saveComment(this.state.comment);
        this.setState({comment: ''});
    }

    render(){
        return(
            <div>
                <form onSubmit={this.handleSubmit}>
                    <h4>Add a comment</h4>
                    <textarea name="" id="" cols="30" rows="10" value={this.state.comment} onChange={this.handleCange}></textarea>
                    <div>
                        <button>Submit Comment</button>
                    </div>
                </form>
                <button className="fetch_comments" onClick={this.props.fetchComments}>Fetch Comments</button>
            </div>
        );
    }
}

export default connect(null, {saveComment, fetchComments})(CommentBox);